/**
 * Simple Read
 * 
 * Read data from the serial port and change the color of a rectangle
 */

// Import Serial Class for processing
import processing.serial.*;

// Declare Serial class

Serial myPort;    // Create object from Serial class
int input;        // Input readings that we will be reading
int end = 10;     // ASCII character for new line

// Server we are sending our data to
String server = "https://iot-mdm.herokuapp.com";

// Modify this to create a new app name
String appName = "kjchabra"; 

void setup() 
{
  size(200, 200);
  // I know that the first port in the serial list on my mac
  // is always my  FTDI adaptor, so I open Serial.list()[0].
  // On Windows machines, this generally opens COM1.
  // Open whatever port is the one you're using.
  String portName = Serial.list()[1];
  myPort = new Serial(this, portName, 9600);
  noStroke();
}

void draw() {
  // Create a local variable that stores previous input value
  int prevInput = input;

  // If data is available,
  if ( myPort.available() > 0) {

    // read it and store it as a string
    String val = myPort.readStringUntil(end);  

    // check if value actually exists
    if (val != null) {

      //Convert String value to whole number
      input = Integer.parseInt(trim(val) );

      //Map the min and max value to 0 to 255;
      input = int(map(input, 0, 500, 0, 255) );
    }
  }

  // Don't send value again and again
  if (input != prevInput) {
    //create a curl command to send signal value to server
    String[] curl = loadStrings(server+"/app/"+appName+"/"+str(input) );

    //Print out what we recieve from the server
    for (int i = 0; i < curl.length; i++) {
      println(curl[i]);
    }
  }


  // Set background to white
  background(input);  

  /* create a square 100 x 100 
   and place if 50 pixels to left
   and 50px to right 
   */
  rect(50, 50, 100, 100);
}